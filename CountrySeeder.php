<?php

namespace Database\Seeders;

use App\Models\External\Country;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'goes_id' => '9320',
            'name' => 'ANGUILA',
        ]);

        Country::create([
            'goes_id' => '9539',
            'name' => 'ISLAS TURCAS Y CAICOS',
        ]);

        Country::create([
            'goes_id' => '9565',
            'name' => 'LITUANIA',
        ]);

        Country::create([
            'goes_id' => '9905',
            'name' => 'DAKOTA DEL SUR (USA)',
        ]);

        Country::create([
            'goes_id' => '9999',
            'name' => 'No definido en migración',
        ]);

        Country::create([
            'goes_id' => '9303',
            'name' => 'AFGANISTÁN',
        ]);

        Country::create([
            'goes_id' => '9306',
            'name' => 'ALBANIA',
        ]);

        Country::create([
            'goes_id' => '9309',
            'name' => 'ALEMANIA OCCID',
        ]);

        Country::create([
            'goes_id' => '9310',
            'name' => 'ALEMANIA ORIENT',
        ]);

        Country::create([
            'goes_id' => '9315',
            'name' => 'ALTO VOLTA',
        ]);

        Country::create([
            'goes_id' => '9317',
            'name' => 'ANDORRA',
        ]);

        Country::create([
            'goes_id' => '9318',
            'name' => 'ANGOLA',
        ]);

        Country::create([
            'goes_id' => '9319',
            'name' => 'ANTIG Y BARBUDA',
        ]);

        Country::create([
            'goes_id' => '9324',
            'name' => 'ARABIA SAUDITA',
        ]);

        Country::create([
            'goes_id' => '9327',
            'name' => 'ARGELIA',
        ]);

        Country::create([
            'goes_id' => '9330',
            'name' => 'ARGENTINA',
        ]);

        Country::create([
            'goes_id' => '9333',
            'name' => 'AUSTRALIA',
        ]);

        Country::create([
            'goes_id' => '9336',
            'name' => 'AUSTRIA',
        ]);

        Country::create([
            'goes_id' => '9339',
            'name' => 'BANGLADESH',
        ]);

        Country::create([
            'goes_id' => '9342',
            'name' => 'BAHRÉIN',
        ]);

        Country::create([
            'goes_id' => '9345',
            'name' => 'BARBADOS',
        ]);

        Country::create([
            'goes_id' => '9348',
            'name' => 'BÉLGICA',
        ]);

        Country::create([
            'goes_id' => '9349',
            'name' => 'BELICE',
        ]);

        Country::create([
            'goes_id' => '9350',
            'name' => 'BENÍN',
        ]);

        Country::create([
            'goes_id' => '9354',
            'name' => 'BIRMANIA',
        ]);

        Country::create([
            'goes_id' => '9357',
            'name' => 'BOLIVIA',
        ]);

        Country::create([
            'goes_id' => '9360',
            'name' => 'BOTSWANA',
        ]);

        Country::create([
            'goes_id' => '9363',
            'name' => 'BRASIL',
        ]);

        Country::create([
            'goes_id' => '9366',
            'name' => 'BRUNÉI',
        ]);

        Country::create([
            'goes_id' => '9372',
            'name' => 'BURUNDI',
        ]);

        Country::create([
            'goes_id' => '9374',
            'name' => 'BOPHUTHATSWANA',
        ]);

        Country::create([
            'goes_id' => '9375',
            'name' => 'BUTÁN',
        ]);

        Country::create([
            'goes_id' => '9377',
            'name' => 'CABO VERDE',
        ]);

        Country::create([
            'goes_id' => '9378',
            'name' => 'CAMBOYA',
        ]);

        Country::create([
            'goes_id' => '9381',
            'name' => 'CAMERÚN',
        ]);

        Country::create([
            'goes_id' => '9384',
            'name' => 'CANADÁ',
        ]);

        Country::create([
            'goes_id' => '',
            'name' => '',
        ]);
    }
}
