<?php

namespace Database\Seeders;

use App\Models\External\EconomicActivity;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EconomicActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EconomicActivity::create([
            'goes_id' => '01111',
            'name' => 'Cultivo de cereales excepto arroz y para forrajes',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01112',
            'name' => 'Cultivo de legumbres',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01113',
            'name' => 'Cultivo de semillas oleaginosas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01114',
            'name' => 'Cultivo de plantas para la preparación de semillas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01119',
            'name' => 'Cultivo de otros cereales excepto arroz y forrajeros n.c.p.',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01120',
            'name' => 'Cultivo de arroz',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01131',
            'name' => 'Cultivo de raíces y tubérculos ',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01132',
            'name' => 'Cultivo de brotes, bulbos, vegetales tubérculos y cultivos similares',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01133',
            'name' => 'Cultivo hortícola de fruto',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01134',
            'name' => 'Cultivo de hortalizas de hoja y otras hortalizas ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01140',
            'name' => 'Cultivo de caña de azúcar',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01150',
            'name' => 'Cultivo de tabaco ',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01161',
            'name' => 'Cultivo de algodón',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01162',
            'name' => 'Cultivo de fibras vegetales excepto algodón',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01191',
            'name' => 'Cultivo de plantas no perennes para la producción de semillas y flores',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01192',
            'name' => 'Cultivo de cereales y pastos para la alimentación animal',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01199',
            'name' => 'Producción de cultivos no estacionales ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01220',
            'name' => 'Cultivo de frutas tropicales',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01230',
            'name' => 'Cultivo de cítricos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01240',
            'name' => 'Cultivo de frutas de pepita y hueso',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01251',
            'name' => 'Cultivo de frutas ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01252',
            'name' => 'Cultivo de otros frutos y nueces de árboles y arbustos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01260',
            'name' => 'Cultivo de frutos oleaginosos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01271',
            'name' => 'Cultivo de café',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01272',
            'name' => 'Cultivo de plantas para la elaboración de bebidas excepto café',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01281',
            'name' => 'Cultivo de especias y aromáticas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01282',
            'name' => 'Cultivo de plantas para la obtención de productos medicinales y farmacéuticos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01291',
            'name' => 'Cultivo de árboles de hule (caucho) para la obtención de látex',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01292',
            'name' => 'Cultivo de plantas para la obtención de productos químicos y colorantes',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01299',
            'name' => 'Producción de cultivos perennes ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01300',
            'name' => 'Propagación de plantas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01301',
            'name' => 'Cultivo de plantas y flores ornamentales',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01410',
            'name' => 'Cría y engorde de ganado bovino',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01420',
            'name' => 'Cría de caballos y otros equinos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01440',
            'name' => 'Cría de ovejas y cabras',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01450',
            'name' => 'Cría de cerdos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01460',
            'name' => 'Cría de aves de corral y producción de huevos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01491',
            'name' => 'Cría de abejas apicultura para la obtención de miel y otros productos apícolas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01492',
            'name' => 'Cría de conejos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01493',
            'name' => 'Cría de iguanas y garrobos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01494',
            'name' => 'Cría de mariposas y otros insectos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01499',
            'name' => 'Cría y obtención de productos animales n.c.p.',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01500',
            'name' => 'Cultivo de productos agrícolas en combinación con la cría de animales',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01611',
            'name' => 'Servicios de maquinaria agrícola',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01612',
            'name' => 'Control de plagas',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01613',
            'name' => 'Servicios de riego',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01614',
            'name' => 'Servicios de contratación de mano de obra para la agricultura',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01619',
            'name' => 'Servicios agrícolas ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01621',
            'name' => 'Actividades para mejorar la reproducción, el crecimiento y el rendimiento de los animales y sus productos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01622',
            'name' => 'Servicios de mano de obra pecuaria',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01629',
            'name' => 'Servicios pecuarios ncp',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01631',
            'name' => 'Labores post cosecha de preparación de los productos agrícolas para su comercialización o para la industria',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01632',
            'name' => 'Servicio de beneficio de café',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01633',
            'name' => 'Servicio de beneficiado de plantas textiles (incluye el beneficiado cuando este es realizado en la misma explotación agropecuaria) ',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01640',
            'name' => 'Tratamiento de semillas para la propagación',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '01700',
            'name' => 'Caza ordinaria y mediante trampas, repoblación de animales de caza y servicios conexos',
            'classification' => 1,
        ]);

        EconomicActivity::create([
            'goes_id' => '02100',
            'name' => 'Silvicultura y otras actividades forestales',
            'classification' => 2,
        ]);

        EconomicActivity::create([
            'goes_id' => '02200',
            'name' => 'Extracción de madera',
            'classification' => 2,
        ]);

        EconomicActivity::create([
            'goes_id' => '02300',
            'name' => 'Recolección de productos diferentes a la madera',
            'classification' => 2,
        ]);

        EconomicActivity::create([
            'goes_id' => '02400',
            'name' => 'Servicios de apoyo a la silvicultura',
            'classification' => 2,
        ]);

        EconomicActivity::create([
            'goes_id' => '03110',
            'name' => 'Pesca marítima de altura y costera',
            'classification' => 3,
        ]);

        EconomicActivity::create([
            'goes_id' => '03120',
            'name' => 'Pesca de agua dulce',
            'classification' => 3,
        ]);

        EconomicActivity::create([
            'goes_id' => '03210',
            'name' => 'Acuicultura marítima',
            'classification' => 3,
        ]);

        EconomicActivity::create([
            'goes_id' => '03220',
            'name' => 'Acuicultura de agua dulce ',
            'classification' => 3,
        ]);

        EconomicActivity::create([
            'goes_id' => '03300',
            'name' => 'Servicios de apoyo a la pesca y acuicultura',
            'classification' => 3,
        ]);

        EconomicActivity::create([
            'goes_id' => '05100',
            'name' => 'Extracción de hulla',
            'classification' => 4,
        ]);

        EconomicActivity::create([
            'goes_id' => '05200',
            'name' => 'Extracción y aglomeración de lignito',
            'classification' => 4,
        ]);

        EconomicActivity::create([
            'goes_id' => '06100',
            'name' => 'Extracción de petróleo crudo',
            'classification' => 5,
        ]);

        EconomicActivity::create([
            'goes_id' => '06200',
            'name' => 'Extracción de gas natural',
            'classification' => 5,
        ]);

        EconomicActivity::create([
            'goes_id' => '07100',
            'name' => 'Extracción de minerales de hierro',
            'classification' => 6,
        ]);

        EconomicActivity::create([
            'goes_id' => '07210',
            'name' => 'Extracción de minerales de uranio y torio',
            'classification' => 6,
        ]);

        EconomicActivity::create([
            'goes_id' => '07290',
            'name' => 'Extracción de minerales metalíferos no ferrosos',
            'classification' => 6,
        ]);

        EconomicActivity::create([
            'goes_id' => '08100',
            'name' => 'Extracción de piedra, arena y arcilla',
            'classification' => 7,
        ]);

        EconomicActivity::create([
            'goes_id' => '08910',
            'name' => 'Extracción de minerales para la fabricación de abonos y productos químicos',
            'classification' => 7,
        ]);

        EconomicActivity::create([
            'goes_id' => '08920',
            'name' => 'Extracción y aglomeración de turba',
            'classification' => 7,
        ]);

        EconomicActivity::create([
            'goes_id' => '08930',
            'name' => 'Extracción de sal',
            'classification' => 7,
        ]);

        EconomicActivity::create([
            'goes_id' => '08990',
            'name' => 'Explotación de otras minas y canteras ncp',
            'classification' => 7,
        ]);

        EconomicActivity::create([
            'goes_id' => '09100',
            'name' => 'Actividades de apoyo a la extracción de petróleo y gas natural',
            'classification' => 8,
        ]);

        EconomicActivity::create([
            'goes_id' => '09900',
            'name' => 'Actividades de apoyo a la explotación de minas y canteras',
            'classification' => 8,
        ]);

        EconomicActivity::create([
            'goes_id' => '10101',
            'name' => 'Servicio de rastros y mataderos de bovinos y porcinos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10102',
            'name' => 'Matanza y procesamiento de bovinos y porcinos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10103',
            'name' => 'Matanza y procesamientos de aves de corral',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10104',
            'name' => 'Elaboración y conservación de embutidos y tripas naturales',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10105',
            'name' => 'Servicios de conservación y empaque de carnes',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10106',
            'name' => 'Elaboración y conservación de grasas y aceites animales',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10107',
            'name' => 'Servicios de molienda de carne',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10108',
            'name' => 'Elaboración de productos de carne ncp',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10201',
            'name' => 'Procesamiento y conservación de pescado, crustáceos y moluscos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10209',
            'name' => 'Fabricación de productos de pescado ncp',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10301',
            'name' => 'Elaboración de jugos de frutas y hortalizasv',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10302',
            'name' => 'Elaboración y envase de jaleas, mermeladas y frutas deshidratadas',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10309',
            'name' => 'Elaboración de productos de frutas y hortalizas n.c.p.',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10401',
            'name' => 'Fabricación de aceites y grasas vegetales y animales comestibles',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10402',
            'name' => 'Fabricación de aceites y grasas vegetales y animales no comestibles',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10409',
            'name' => 'Servicio de maquilado de aceites',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10501',
            'name' => 'Fabricación de productos lácteos excepto sorbetes y quesos sustitutos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10502',
            'name' => 'Fabricación de sorbetes y helados',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10503',
            'name' => 'Fabricación de quesos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10611',
            'name' => 'Molienda de cereales',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10612',
            'name' => 'Elaboración de cereales para el desayuno y similares',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10613',
            'name' => 'Servicios de beneficiado de productos agrícolas ncp',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10621',
            'name' => 'Fabricación de almidón',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10628',
            'name' => 'Servicio de molienda de maíz húmedo molino para nixtamal',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10711',
            'name' => 'Elaboración de tortillas ',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10712',
            'name' => 'Fabricación de pan, galletas y barquillos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10713',
            'name' => 'Fabricación de repostería',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10721',
            'name' => 'Ingenios azucareros',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10722',
            'name' => 'Molienda de caña de azúcar para la elaboración de dulces',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10723',
            'name' => 'Elaboración de jarabes de azúcar y otros similares',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10724',
            'name' => 'Maquilado de azúcar de caña',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10730',
            'name' => 'Fabricación de cacao, chocolates y productos de confitería',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10740',
            'name' => 'Elaboración de macarrones, fideos, y productos farináceos similares',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10750',
            'name' => 'Elaboración de comidas y platos preparados para la reventa en locales y/o para exportación ',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10791',
            'name' => 'Elaboración de productos de café',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10792',
            'name' => 'Elaboración de especies, sazonadores y condimentos',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '10793',
            'name' => 'Elaboración de sopas, cremas y consomé',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '',
            'name' => '',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '',
            'name' => '',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '',
            'name' => '',
            'classification' => 9,
        ]);

        EconomicActivity::create([
            'goes_id' => '',
            'name' => '',
            'classification' => 9,
        ]);
    }
}
